﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace akasmigrate.Helpers
{
    public static class FileHelper
    {
        public static string GetText(string path)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var data = File.ReadAllBytes(path);
            var isUtf8 = IsUtf8(data);
            string result = null;
            if (isUtf8)
            {
                if (IsUtf8WithBom(data))
                {
                    data = data.Skip(3).ToArray();
                }
                result = Encoding.UTF8.GetString(data);
            }
            else
            {
                result = Encoding.GetEncoding(1251).GetString(data);
            }
            return result;
        }

        public static bool IsUtf8WithBom(byte[] content)
        {
            if (content == null)
                throw new NullReferenceException(nameof(content));
            if (content.Length > 3
                && content[0] == 0xEF
                && content[1] == 0xBB
                && content[2] == 0xBF)
            {
                return true;
            }
            return false;
        }

        public static bool IsUtf8(byte[] content)
        {
            if (content == null)
                throw new NullReferenceException(nameof(content));
            if (IsUtf8WithBom(content))
            {
                return true;
            }

            int chainsCorrent = 0;
            int chainsWrong = 0;

            bool isUtf8Chain = false;
            int symbsInChainRemaining = 0;

            foreach (var c in content)
            {
                var utf8_2b = (c & 0b1110_0000) == 0b1100_0000; // 110x xxxx
                var utf8_3b = (c & 0b1111_0000) == 0b1110_0000; // 1110 xxxx
                var utf8_4b = (c & 0b1111_1000) == 0b1111_0000; // 1111 0xxx
                var utf8_ch = (c & 0b1100_0000) == 0b1000_0000; // 10xx xxxx

                if (!isUtf8Chain && utf8_2b)
                {
                    isUtf8Chain = true;
                    symbsInChainRemaining = 1;
                    continue;
                }
                else if (!isUtf8Chain && utf8_3b)
                {
                    isUtf8Chain = true;
                    symbsInChainRemaining = 2;
                    continue;
                }
                else if (!isUtf8Chain && utf8_4b)
                {
                    isUtf8Chain = true;
                    symbsInChainRemaining = 3;
                    continue;
                }

                if (symbsInChainRemaining > 0)
                {
                    if (!utf8_ch)
                    {
                        isUtf8Chain = false;
                        symbsInChainRemaining = 0;
                        ++chainsWrong;
                        break;
                    }
                    else
                    {
                        --symbsInChainRemaining;
                    }
                }

                if (symbsInChainRemaining == 0 && isUtf8Chain)
                {
                    isUtf8Chain = false;
                    ++chainsCorrent;
                }
            }
            return chainsCorrent > 0 && chainsWrong == 0;
        }
    }
}
