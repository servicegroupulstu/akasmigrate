﻿using System;

namespace akasmigrate.Helpers
{
    public static class LogHelper
    {
        private static readonly DateTime time = DateTime.Now;

        public static void Info(string message)
        {
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write($"[INFO - {time.ToLongTimeString()}]");

            Console.ResetColor();
            Console.WriteLine($" {message}");

        }

        public static void Error(string message)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write($"[ERROR - {time.ToLongTimeString()}]");

            Console.ResetColor();
            Console.WriteLine($" {message}");
        }
    }
}
