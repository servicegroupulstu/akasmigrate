﻿using akasmigrate.Helpers;
using akasmigrate.Models;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace akasmigrate.Steps
{
    public static class Init
    {
        public static Env Run()
        {
            var env = new Env();
            env.RootPath = GetAppRootPath();
            env.MigrationsPath = "Migrations";

            string configJson = File.ReadAllText(Path.Combine(env.RootPath, "config.json"));
            var config = JsonConvert.DeserializeObject<Config>(configJson);
            env.Config = config;

            env.Db = new DB(config);

            return env;
        }

        static string GetAppRootPath()
        {
            var exePath = Path.GetDirectoryName(System.Reflection
                                .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            return appPathMatcher.Match(exePath).Value;
        }
    }
}
