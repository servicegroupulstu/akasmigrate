﻿using akasmigrate.Helpers;
using akasmigrate.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace akasmigrate.Steps
{
    public static class Migration
    {
        public static void Run(Env env)
        {
            // Имя таблицы для лога миграций.
            var migrationTableName = "public.migration_log";

            // Создаем таблицу для лога миграций, если её нет в БД.
            env.Db.CreateMigrationLogTable(migrationTableName);

            // Получаем лог выполненных миграций.
            var migrationLogs = env.Db.GetMigrationLog(migrationTableName);

            // Получаем все файлы миграций.
            Directory.SetCurrentDirectory(Path.Combine(env.RootPath, env.MigrationsPath));
            var allFiles = Directory.EnumerateFiles(".", "*.sql", SearchOption.AllDirectories);

            // Отбираем новые файлы для миграций.
            var newFiles = new List<MigrationLog>();
            foreach (var file in allFiles)
            {
                string fileName = Path.GetFileName(file);
                string dirName = Path.GetDirectoryName(file);

                bool isNew = !migrationLogs.Any(d => d.FileName == fileName);
                if (isNew)
                    newFiles.Add(new MigrationLog
                    {
                        FileName = fileName
                    });
            }
            newFiles = newFiles.OrderBy(d => d.FileName).ToList();

            // Выполняем миграции.
            foreach (var item in newFiles)
            {
                try
                {
                    var text = FileHelper.GetText(item.FileName);
                    env.Db.ExecScript(text);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + "  Error.");
                }

                try
                {
                    env.Db.Save(migrationTableName, item);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message, "  Can't save log.");
                }
            }
        }
    }
}
