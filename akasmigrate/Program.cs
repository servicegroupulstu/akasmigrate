﻿using akasmigrate.Steps;

namespace akasmigrate
{
    class Program
    {
        static void Main(string[] args)
        {
            // Устанавливаем конфиг и инциализируем БД.
            var env = Init.Run();

            // Выполняем миграции.
            Migration.Run(env);
        }
    }
}
