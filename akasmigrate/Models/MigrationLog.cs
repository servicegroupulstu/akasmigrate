﻿using System;
using System.Data;

namespace akasmigrate.Models
{
    public class MigrationLog
    {
        public string FileName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public static MigrationLog FromDataRow(DataRow row)
        {
            var item = new MigrationLog
            {
                FileName = row.Field<string>("migration_id"),
                Created = row.Field<DateTime>("сreated_at"),
                Updated = row.Field<DateTime>("updated_at")
            };

            return item;
        }
    }
}
