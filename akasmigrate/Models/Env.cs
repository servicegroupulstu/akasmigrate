﻿namespace akasmigrate.Models
{
    public class Env
    {
        public DB Db { get; set; }
        public Config Config { get; set; }
        public string MigrationsPath { get; set; }
        public string RootPath { get; set; }
    }
}
