﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace akasmigrate.Models
{
    public class DB
    {
        NpgsqlConnection db;

        public DB(Config config)
        {
            db = new NpgsqlConnection($"Host={config.Host};Port={config.Port};Database={config.Database};Username={config.Username};Password={config.Password}");
        }

        public void ExecScript(string sql)
        {
            try
            {
                // cleanup comments
                sql = Regex.Replace(sql, @"^\s*?--(.*?)$", "", RegexOptions.Compiled | RegexOptions.Multiline);
                sql = Regex.Replace(sql, @"/\*(.*?)\*/", "", RegexOptions.Compiled | RegexOptions.Multiline);

                // exec
                var cmd = new NpgsqlCommand
                {
                    Connection = db,
                    CommandTimeout = 0
                };
                db.Open();

                foreach (var query in Regex.Split(sql, @"^\s*GO\s*$", RegexOptions.Compiled | RegexOptions.Multiline).Where(d => !string.IsNullOrWhiteSpace(d)))
                {
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (AggregateException e)
            {
                var sb = new StringBuilder();
                foreach (var ie in e.InnerExceptions)
                {
                    if (sb.Length > 0)
                        sb.Append("; ");
                    sb.Append(ie.GetType().Name + ": " + ie.Message);
                }
                sb.Insert(0, "Errors: ");
                Console.WriteLine(sb.ToString());
                throw;
            }
            catch (Exception e)
            {
                var sb = new StringBuilder();
                var ie = e;
                while (ie != null)
                {
                    if (sb.Length > 0)
                        sb.Append("; ");
                    sb.Append(ie.GetType().Name + ": " + ie.Message);
                    ie = ie.InnerException;
                }
                Console.WriteLine(sb.ToString());
                throw;
            }
            finally
            {
                db.Close();
            }
        }

        public void ExecNonQuery(string sql, params NpgsqlParameter[] sqlParameters)
        {
            try
            {
                var cmd = new NpgsqlCommand
                {
                    Connection = db,
                    CommandTimeout = 0
                };
                db.Open();

                cmd.CommandText = sql;
                foreach (var sqlParameter in sqlParameters)
                    cmd.Parameters.Add(sqlParameter);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                db.Close();
            }
        }

        public List<MigrationLog> GetMigrationLog(string tableName)
        {
            var list = new List<MigrationLog>();
            var cmd = new NpgsqlCommand("select * from " + tableName + " order by updated_at", db);

            var reader = new NpgsqlDataAdapter(cmd);
            var dt = new System.Data.DataTable();
            reader.Fill(dt);
            for (var i = 0; i < dt.Rows.Count; i++)
                list.Add(MigrationLog.FromDataRow(dt.Rows[i]));

            return list;
        }

        public void Save(string tableName, MigrationLog item)
        {
            var cmd = new NpgsqlCommand(
                "INSERT INTO " + tableName + " (migration_id, сreated_at, updated_at)"
                + "SELECT @p0, @p1, @p2",
                db
                );
            cmd.Parameters.AddWithValue("@p0", item.FileName);
            cmd.Parameters.AddWithValue("@p1", DateTimeOffset.Now);
            cmd.Parameters.AddWithValue("@p2", DateTimeOffset.Now);

            db.Open();
            cmd.ExecuteNonQuery();
            db.Close();
        }

        public void CreateMigrationLogTable(string tableName)
        {
            try
            {
                var cmd = new NpgsqlCommand
                {
                    Connection = db,
                    CommandTimeout = 0
                };
                db.Open();

                cmd.CommandText = "create table if not exists " + tableName +
                    " (" +
                        " migration_id character varying(150) COLLATE pg_catalog.default NOT NULL," +
                        " сreated_at timestamp NOT NULL," +
                        " updated_at timestamp NOT NULL," +
                        " CONSTRAINT pk_" + tableName.Split('.').Last() + " PRIMARY KEY(migration_id)" +
                    " );";
                cmd.ExecuteNonQuery();
            }
            finally
            {
                db.Close();
            }
        }
    }
}
