CREATE SCHEMA IF NOT EXISTS main;

CREATE TABLE IF NOT EXISTS main.roles (
    id              SERIAL          NOT NULL
        CONSTRAINT pk_main_roles PRIMARY KEY,

    code            VARCHAR(16)     NOT NULL,
    "name"          VARCHAR(32)     NOT NULL,
    "description"   TEXT,

    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.faculties (
    id              SERIAL          NOT NULL
        CONSTRAINT pk_main_faculties PRIMARY KEY,
    
    short_name      VARCHAR(16)     NOT NULL,   
    full_name       VARCHAR(32)     NOT NULL, 
    
    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.departments (
    id              SERIAL          NOT NULL
        CONSTRAINT pk_main_departments PRIMARY KEY,
    
    short_name      VARCHAR(64)     NOT NULL,   
    full_name       TEXT            NOT NULL,
    faculty_id      INTEGER         NOT NULL
        CONSTRAINT fk_main_departments_faculties
            REFERENCES main.faculties
                ON UPDATE CASCADE ON DELETE NO ACTION,
    
    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.groups (
    id              SERIAL          NOT NULL
        CONSTRAINT pk_main_groups PRIMARY KEY,
    
    short_name      VARCHAR(64)     NOT NULL,   
    full_name       TEXT            NOT NULL,
    department_id   INTEGER         NOT NULL
        CONSTRAINT fk_main_groups_departments 
            REFERENCES main.departments
                ON UPDATE CASCADE ON DELETE NO ACTION,
    
    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.users (
    id              SERIAL       NOT NULL
        CONSTRAINT pk_main_users PRIMARY KEY,

    "login"         VARCHAR(64)     NOT NULL    UNIQUE,
    email           VARCHAR(64)     NOT NULL    UNIQUE,
    phone_number    VARCHAR(32)     NOT NULL    UNIQUE,
    "password"      VARCHAR(256)    NOT NULL,
    "name"          VARCHAR(128)    NOT NULL,
    surname         VARCHAR(128)    NOT NULL,
    avatar          VARCHAR(128),
    role_id         INTEGER         NOT NULL
        CONSTRAINT fk_main_users_roles 
                REFERENCES main.roles
                ON UPDATE CASCADE ON DELETE NO ACTION,
                
    group_id        INTEGER
        CONSTRAINT fk_main_users_groups 
            REFERENCES main.groups
                ON UPDATE CASCADE ON DELETE NO ACTION,

    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.subjects (
    id              SERIAL          NOT NULL
        CONSTRAINT pk_main_subjects PRIMARY KEY,
    
    user_id         INTEGER         NOT NULL
        CONSTRAINT fk_main_subjects_users 
            REFERENCES main.users
                ON UPDATE CASCADE ON DELETE NO ACTION,

    "name"           TEXT           NOT NULL,
    "description"    TEXT,
    exam_type        TEXT,
    exam_condition   TEXT,

    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.semesters (
    id              SERIAL      NOT NULL
        CONSTRAINT pk_main_semesters PRIMARY KEY,
    
    begin_year      INTEGER     NOT NULL,
    end_year        INTEGER     NOT NULL,

    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
GO

CREATE TABLE IF NOT EXISTS main.teachings (
    id              SERIAL          NOT NULL
        CONSTRAINT pk_main_teachings PRIMARY KEY,

    semester_id     INTEGER         NOT NULL
        CONSTRAINT fk_main_teachings_semesters 
            REFERENCES main.semesters
                ON UPDATE CASCADE ON DELETE NO ACTION,

    group_id        INTEGER         NOT NULL
        CONSTRAINT fk_main_teachings_groups 
            REFERENCES main.groups
                ON UPDATE CASCADE ON DELETE NO ACTION,

    subject_id      INTEGER         NOT NULL
        CONSTRAINT fk_main_teachings_subjects 
            REFERENCES main.subjects
                ON UPDATE CASCADE ON DELETE NO ACTION,

    created_at      TIMESTAMPTZ     NOT NULL    DEFAULT (NOW() AT TIME ZONE 'UTC'),
    updated_at      TIMESTAMPTZ
);
