CREATE SCHEMA IF NOT EXISTS testing;

CREATE TABLE IF NOT EXISTS testing.question
(
  id serial NOT NULL,
  complexity double precision,
  question text,
  type character varying(255),
  subject_id integer,
  CONSTRAINT question_pkey PRIMARY KEY (id),
  FOREIGN KEY (subject_id) 
      REFERENCES main.subjects (id)
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE IF NOT EXISTS testing.answer_choice
(
  id serial NOT NULL,
  answer character varying(255),
  is_right boolean,
  question_id integer
  CONSTRAINT answer_choice_pkey PRIMARY KEY (id),
  FOREIGN KEY (question_id)
      REFERENCES testing.question (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
