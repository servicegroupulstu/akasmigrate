CREATE SCHEMA IF NOT EXISTS schedule;

CREATE TABLE IF NOT EXISTS schedule.group_student
(
    id bigint NOT NULL DEFAULT nextval('group_student_id_seq'::regclass),
    name_group character varying(255) COLLATE pg_catalog."default",
    url_group character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT group_student_pkey PRIMARY KEY (id)
)
GO

CREATE TABLE IF NOT EXISTS schedule.day
(
    id bigint NOT NULL DEFAULT nextval('day_id_seq'::regclass),
    group_id bigint,
    number_week integer,
    number_day integer,
    CONSTRAINT day_pkey PRIMARY KEY (id),
    CONSTRAINT day_group_fkey FOREIGN KEY (group_id)
        REFERENCES schedule.group_student (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
GO

CREATE TABLE IF NOT EXISTS schedule.couple
(
    id bigint NOT NULL DEFAULT nextval('couple_id_seq'::regclass),
    day_id bigint,
    info character varying(255) COLLATE pg_catalog."default",
    name_group character varying(255) COLLATE pg_catalog."default",
    number_week integer,
    number_day integer,
    pair_number integer,
    place character varying(255) COLLATE pg_catalog."default",
    subgroup integer,
    subject character varying(255) COLLATE pg_catalog."default",
    teacher character varying(255) COLLATE pg_catalog."default",
    type_subject integer,
    CONSTRAINT couple_pkey PRIMARY KEY (id),
    CONSTRAINT couple_day_fkey FOREIGN KEY (day_id)
        REFERENCES schedule.day (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
